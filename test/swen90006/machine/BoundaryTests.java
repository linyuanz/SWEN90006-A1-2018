package swen90006.machine;

import java.util.List;
import java.util.ArrayList;
import org.junit.*;
import static org.junit.Assert.*;
import org.junit.Rule;
import org.junit.rules.ExpectedException;


public class BoundaryTests
{
  //Any method annotated with "@Before" will be executed before each test,
  //allowing the tester to set up some shared resources.
  @Before public void setUp()
  {
  }

  //Any method annotated with "@After" will be executed after each test,
  //allowing the tester to release any shared resources used in the setup.
  @After public void tearDown()
  {
  }

  //Any method annotation with "@Test" is executed as a test.
@Rule
public ExpectedException thrown= ExpectedException.none();

//ADD R0 R-1 R0;(Off point)
@Test public void Add_Regsiter_Boundary1()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("add r0 r-1 r0;");
    list.add("ret r0;");
    thrown.expect(InvalidInstructionException.class);
    m.execute(list);
}
//ADD R0 R32 R0;(Off point)
@Test public void Add_Regsiter_Boundary2()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("add r0 r32 r0;");
    list.add("ret r0;");
    thrown.expect(InvalidInstructionException.class);
    m.execute(list);
}
//ADD R0 R0 R31;(On point)
@Test
public void Add_Regsiter_Boundary3(){
   try{
      //execute code that you expect not to throw Exceptions.
      List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("add r0 r31 r0;");
    list.add("ret r0;");
    m.execute(list);
   }
   catch(InvalidInstructionException | NoReturnValueException e){
      fail("Should not have thrown any exception");
   }
}
@Test public void ADD_Value_Boundary1()
{ 
      //execute code that you expect not to throw Exceptions.
    List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("MOV R0 1;");
    list.add("MOV R1 2;");
    list.add("MOV R2 1;");
    list.add("ADD R1 R1 R2;");
    list.add("RET R1;");
    assertEquals(3, m.execute(list));
}
@Test public void ADD_X()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("ADD R0;");
    thrown.expect(InvalidInstructionException.class);
    m.execute(list);
}
//SUB R0 R32 R0;(Off point)
@Test public void Sub_Regsiter_Boundary1()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("sub r0 r32 r0;");
    list.add("ret r0;");
    thrown.expect(InvalidInstructionException.class);
    m.execute(list);
}
//SUB R0 R-1 R0;(Off point)
@Test public void Sub_Regsiter_Boundary2()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("Sub r0 r-1 r0;");
    list.add("ret r0;");
    thrown.expect(InvalidInstructionException.class);
    m.execute(list);
}
//SUB R0 R0 R31;(On point)
@Test public void Sub_Regsiter_Boundary3(){
   try{
      //execute code that you expect not to throw Exceptions.
    List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("sub r0 r31 r0;");
    list.add("ret r0;");
    m.execute(list);
   }
   catch(InvalidInstructionException | NoReturnValueException e){
      fail("Should not have thrown any exception");
   }
}
@Test public void Sub_Value_Boundary1()
{ 
      //execute code that you expect not to throw Exceptions.
    List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("MOV R0 1;");
    list.add("MOV R1 2;");
    list.add("MOV R2 1;");
    list.add("SUB R1 R1 R2;");
    list.add("RET R1;");
    assertEquals(1, m.execute(list));
}
@Test public void SUB_X()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("SUB R0;");
    thrown.expect(InvalidInstructionException.class);
    m.execute(list);
}
//Mul R0 R32 R0;(Off point)
@Test public void Mul_Regsiter_Boundary1()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("Mul r0 r32 r0;");
    list.add("ret r0;");
    thrown.expect(InvalidInstructionException.class);
    m.execute(list);
}
//SUB R0 R-1 R0;(Off point)
@Test public void Mul_Regsiter_Boundary2()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("Mul r0 r-1 r0;");
    list.add("ret r0;");
    thrown.expect(InvalidInstructionException.class);
    m.execute(list);
}
//SUB R0 R0 R31;(On point)
@Test public void Mul_Regsiter_Boundary3(){
   try{
      //execute code that you expect not to throw Exceptions.
    List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("Mul r0 r31 r0;");
    list.add("ret r0;");
    m.execute(list);
   }
   catch(InvalidInstructionException | NoReturnValueException e){
      fail("Should not have thrown any exception");
   }
}
@Test public void Mul_Value_Boundary1()
{ 
      //execute code that you expect not to throw Exceptions.
    List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("MOV R0 1;");
    list.add("MOV R1 2;");
    list.add("MOV R2 1;");
    list.add("Mul R1 R1 R2;");
    list.add("RET R1;");
    assertEquals(2, m.execute(list));
}
@Test public void MUL_X()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("MUL R0;");
    thrown.expect(InvalidInstructionException.class);
    m.execute(list);
}
//Div R0 R32 R0;(Off point)
@Test public void Div_Regsiter_Boundary1()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("Div r0 r32 r0;");
    list.add("ret r0;");
    thrown.expect(InvalidInstructionException.class);
    m.execute(list);
}
//Div R0 R-1 R0;(Off point)
@Test public void Div_Regsiter_Boundary2()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("Div r0 r-1 r0;");
    list.add("ret r0;");
    thrown.expect(InvalidInstructionException.class);
    m.execute(list);
}
//DIV R0 R0 R31;(On point)
@Test public void Div_Regsiter_Boundary3(){
   try{
      //execute code that you expect not to throw Exceptions.
    List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("mov r0 2;");
    list.add("div r0 r31 r0;");
    list.add("ret r0;");
    m.execute(list);
   }
   catch(InvalidInstructionException | NoReturnValueException e){
      fail("Should not have thrown any exception");
   }
}
@Test public void Div_Value_Boundary1()
{ 
      //execute code that you expect not to throw Exceptions.
    List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("MOV R0 1;");
    list.add("MOV R1 4;");
    list.add("MOV R2 2;");
    list.add("DIV R1 R1 R2;");
    list.add("RET R1;");
    assertEquals(2, m.execute(list));
}
@Test public void Div_Value_Boundary2()
{ 
      //execute code that you expect not to throw Exceptions.
    List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("MOV R0 1;");
    list.add("MOV R1 4;");
    list.add("MOV R2 2;");
    list.add("DIV R1 R1 R0;");
    list.add("RET R1;");
    assertEquals(4, m.execute(list));
}
@Test public void div_X()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("DIV R0;");
    thrown.expect(InvalidInstructionException.class);
    m.execute(list);
}
//RET R-1;(Off point)
@Test public void Ret_Regsiter_Boundary1()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("ret r-1;");
    thrown.expect(InvalidInstructionException.class);
    m.execute(list);
}
//RET R32;(Off point)
@Test public void Ret_Regsiter_Boundary2()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("ret r32;");
    thrown.expect(InvalidInstructionException.class);
    m.execute(list);
}
//RET R0;(On point)
@Test public void Ret_Regsiter_Boundary3(){
   try{
      //execute code that you expect not to throw Exceptions.
      List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("ret r0;");
    m.execute(list);
   }
   catch(InvalidInstructionException | NoReturnValueException e){
      fail("Should not have thrown any exception");
   }
}
//RET R31;(On point)
@Test public void Ret_Regsiter_Boundary4(){
   try{
      //execute code that you expect not to throw Exceptions.
      List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("ret r31;");
    m.execute(list);
   }
   catch(InvalidInstructionException | NoReturnValueException e){
      fail("Should not have thrown any exception");
   }
}
////ret R0 R31;(Off point)
//@Test public void Ret_Regsiter_4(){
//   try{
//      //execute code that you expect not to throw Exceptions.
//      List<String> list = new ArrayList<>();
//    Machine m =new Machine();
//    list.add("ret R0 R31;");
//    thrown.expect(InvalidInstructionException.class);
//    m.execute(list);
//   }
//   catch(NoReturnValueException e){
//      fail("Should not have thrown any exception");
//   }
//}
@Test public void MOVE_X()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("MOV R0;");
    thrown.expect(InvalidInstructionException.class);
    m.execute(list);
}
//MOV R-1  65535;(Off point)
@Test public void Mov_Regsiter_Boundary1()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("MOV R-1 65535;");
    thrown.expect(InvalidInstructionException.class);
    m.execute(list);
}
//MOV R0 65535;(On point)
@Test public void Mov_Regsiter_Boundary2()
{ 
    try{
      //execute code that you expect not to throw Exceptions.
      List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("MOV R0 65535;");
    list.add("Ret R0;");
    m.execute(list);
   }
   catch(InvalidInstructionException | NoReturnValueException e){
      fail("Should not have thrown any exception");
   }
}
//MOV R31 65535;(On point)
@Test public void Mov_Regsiter_Boundary3()
{ 
    try{
      //execute code that you expect not to throw Exceptions.
      List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("MOV R31 65535;");
    list.add("Ret R0;");
    m.execute(list);
   }
   catch(InvalidInstructionException | NoReturnValueException e){
      fail("Should not have thrown any exception");
   }
}
//MOV R32  65535;(Off point)
@Test public void Mov_Regsiter_Boundary4()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("MOV R32 65535;");
    thrown.expect(InvalidInstructionException.class);
    m.execute(list);
}
//MOV R0 65536;(Off point)
@Test public void Mov_Value_Boundary1()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("MOV R0 65536;");
    thrown.expect(InvalidInstructionException.class);
    m.execute(list);
}
//MOV R0  -65536;(Off point)
@Test public void Mov_Value_Boundary2()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("MOV R0  -65536;");
    thrown.expect(InvalidInstructionException.class);
    m.execute(list);
}
//MOV R0  -65535;(On point)
@Test public void Mov_Value_Boundary3()
{ 
    try{
      //execute code that you expect not to throw Exceptions.
      List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("MOV R0  -65535;");
    list.add("Ret R0;");
    m.execute(list);
   }
   catch(InvalidInstructionException | NoReturnValueException e){
      fail("Should not have thrown any exception");
   }
}
//LDR R1 R-1  65535;(Off point)
@Test public void LDR_Regsiter_Boundary1()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("LDR R1 R-1  65535;");
    thrown.expect(InvalidInstructionException.class);
    m.execute(list);
}
//LDR R1 R0 65535;(On point)
@Test public void LDR_Regsiter_Boundary2()
{ 
    try{
      //execute code that you expect not to throw Exceptions.
      List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("LDR R1 R0 65535;");
    list.add("Ret R1;");
    m.execute(list);
   }
   catch(InvalidInstructionException | NoReturnValueException e){
      fail("Should not have thrown any exception");
   }
}
//LDR R1 R31  65535;(On point)
@Test public void LDR_Regsiter_Boundary3()
{ 
    try{
      //execute code that you expect not to throw Exceptions.
      List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("LDR R1 R31  65535;");
    list.add("Ret R1;");
    m.execute(list);
   }
   catch(InvalidInstructionException | NoReturnValueException e){
      fail("Should not have thrown any exception");
   }
}
//LDR R1 R32  65535;(Off point)
@Test public void LDR_Regsiter_Boundary4()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("LDR R1 R32  65535;");
    thrown.expect(InvalidInstructionException.class);
    m.execute(list);
}
//LDR R1 R0  -65536;(Off point)
@Test public void LDR_Value_Boundary1()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("R1 R0  -65536;");
    thrown.expect(InvalidInstructionException.class);
    m.execute(list);
}
//LDR R1 R0  -65535;(On point)
@Test public void LDR_Value_Boundary2()
{ 
    try{
      //execute code that you expect not to throw Exceptions.
      List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("MOV R0  -65535;");
    list.add("Ret R0;");
    m.execute(list);
   }
   catch(InvalidInstructionException | NoReturnValueException e){
      fail("Should not have thrown any exception");
   }
}
//LDR R1 R0  65536;(On point)
@Test public void LDR_Value_Boundary3()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("R1 R0  65536;");
    list.add("Ret R1;");
    thrown.expect(InvalidInstructionException.class);
    m.execute(list);
}
//b+val<0
@Test public void LDR_Value_Boundary4()
{ 
    try{
      //execute code that you expect not to throw Exceptions.
      List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("MOV R31 1;");
    list.add("LDR R0 R31 -2;");
    list.add("RET R0;");
    m.execute(list);
   }
   catch(InvalidInstructionException | NoReturnValueException e){
      fail("Should not have thrown any exception");
   }
}
//b+val=0
@Test public void LDR_Value_Boundary5()
{ 
    try{
      //execute code that you expect not to throw Exceptions.
      List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("MOV R31 1;");
    list.add("LDR R0 R31 -1;");
    list.add("RET R0;");
    m.execute(list);
   }
   catch(InvalidInstructionException | NoReturnValueException e){
      fail("Should not have thrown any exception");
   }
}
// b+val=65535
@Test public void LDR_Value_Boundary6()
{ 
    try{
      //execute code that you expect not to throw Exceptions.
      List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("MOV R31 0;");
    list.add("LDR R0 R31 65535;");
    list.add("RET R0;");
    m.execute(list);
   }
   catch(InvalidInstructionException | NoReturnValueException e){
      fail("Should not have thrown any exception");
   }
}
// b+val=65536
@Test public void LDR_Value_Boundary7()
{ 
    try{
      //execute code that you expect not to throw Exceptions.
      List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("MOV R31 1;");
    list.add("LDR R0 R31 65535;");
    list.add("RET R0;");
    m.execute(list);
   }
   catch(InvalidInstructionException | NoReturnValueException e){
      fail("Should not have thrown any exception");
   }
}
@Test public void LDR_X()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("LDR R0 1;");
    thrown.expect(InvalidInstructionException.class);
    m.execute(list);
}
//JMP -65536;(Off point)
@Test public void Jmp_Value_Boundary1()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("JMP -65536;");
    thrown.expect(InvalidInstructionException.class);
    m.execute(list);
}
//JMP -65535;(On point)
@Test public void JMP_Value_Boundary2()
{  List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("JMP -65535;");
    thrown.expect(NoReturnValueException.class);
    m.execute(list);
}
//JMP 65535(On point)
@Test public void JMP_Value_Boundary3()
{  List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("JMP 65535;");
    thrown.expect(NoReturnValueException.class);
    m.execute(list);
}
//JMP 65536;(Off point)
@Test public void Jmp_Value_Boundary4()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("JMP 65536;");
    thrown.expect(InvalidInstructionException.class);
    m.execute(list);
}
//JMP pc+val = -1;(Off point)
@Test public void JMP_Value_Boundary5()
{  List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("MOV R0 1;");
    list.add("JMP -2;");
    list.add("RET R0;");
    thrown.expect(NoReturnValueException.class);
    m.execute(list);
}
//JMP pc+val = progLength;(On point)
@Test public void JMP_Value_Boundary6()
{ 
    try{
      //execute code that you expect not to throw Exceptions.
      List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("MOV R0 1;");
    list.add("JMP 1;");
    list.add("RET R0;");
    m.execute(list);
   }
   catch(InvalidInstructionException | NoReturnValueException e){
      fail("Should not have thrown any exception");
   }
}
//JMP pc+val = progLength+1;(Off point)
@Test public void JMP_Value_Boundary7()
{  List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("MOV R0 1;");
    list.add("JMP 2;");
    list.add("RET R0;");
    thrown.expect(NoReturnValueException.class);
    m.execute(list);
}
////jmp 0
//  @Test(timeout = 3000) public void JMP_0() 
//  {
//    List<String> list = new ArrayList<>();
//    Machine m =new Machine();
//    list.add("jmp 0;");
//   thrown.expect(InvalidInstructionException.class);
//    m.execute(list);
//  }
  @Test public void JMP_X()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("JMP R0;");
    thrown.expect(InvalidInstructionException.class);
    m.execute(list);
}
//JZ R-1 65535;(Off point)
@Test public void JZ_Regsiter_Boundary1()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("JZ R-1 65535;");
    thrown.expect(InvalidInstructionException.class);
    m.execute(list);
}
//JZ R0 65535;(On point)
@Test public void JZ_Regsiter_Boundary2()
{ 
    try{
      //execute code that you expect not to throw Exceptions.
      List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("mov R0 1;");
    list.add("JZ R0 65535;");
    list.add("RET R0;");
    m.execute(list);
   }
   catch(InvalidInstructionException | NoReturnValueException e){
      fail("Should not have thrown any exception");
   }
}
@Test public void JZ_Regsiter_Boundary3()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("mov R0 0;");
    list.add("JZ R0 65535;");
    list.add("RET R0;");
    thrown.expect(NoReturnValueException.class);
    m.execute(list);
}
//JZ R31 65535;(On point)
@Test public void JZ_Regsiter_Boundary4()
{ 
    try{
      //execute code that you expect not to throw Exceptions.
      List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("mov R31 1;");
    list.add("JZ R31 65535;");
    list.add("RET R31;");
    m.execute(list);
   }
   catch(InvalidInstructionException | NoReturnValueException e){
      fail("Should not have thrown any exception");
   }
}
@Test public void JZ_Regsiter_Boundary5()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("mov R31 0;");
    list.add("JZ R31 65535;");
    list.add("RET R31;");
    thrown.expect(NoReturnValueException.class);
    m.execute(list);
}
//JZ R32 65535;(Off point)
@Test public void JZ_Regsiter_Boundary6()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("JZ R32 65535;");
    thrown.expect(InvalidInstructionException.class);
    m.execute(list);
}
//JZ R0 65536;(Off point)
@Test public void JZ_Value_Boundary1()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("MOV R0 0;");
    list.add("JZ R0 65536;");
    list.add("RET R31;");
    thrown.expect(InvalidInstructionException.class);
    m.execute(list);
}
//JZ R0 -65535
@Test public void JZ_Value_Boundary2()
{ 
    try{
      //execute code that you expect not to throw Exceptions.
      List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("mov R31 1;");
    list.add("JZ R31 -65535;");
    list.add("RET R31;");
    m.execute(list);
   }
   catch(InvalidInstructionException | NoReturnValueException e){
      fail("Should not have thrown any exception");
   }
}
@Test public void JZ_Value_Boundary3()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("mov R31 0;");
    list.add("JZ R31 -65535;");
    list.add("RET R31;");
    thrown.expect(NoReturnValueException.class);
    m.execute(list);
}
//JZ R0 -65536;(Off point)
@Test public void JZ_Value_Boundary4()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("MOV R0 0;");
    list.add("JZ R0 -65536;");
    list.add("RET R31;");
    thrown.expect(InvalidInstructionException.class);
    m.execute(list);
}
//JZ R0 pc+val = -1;(Off point)
@Test public void JZ_Value_Boundary5()
{  List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("MOV R0 0;");
    list.add("JZ R0 -2;");
    list.add("RET R0;");
    thrown.expect(NoReturnValueException.class);
    m.execute(list);
}
//JZ R0 pc+val = -1;(Off point)
@Test public void JZ_Value_Boundary6()
{ 
    try{
      //execute code that you expect not to throw Exceptions.
      List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("MOV R0 1;");
    list.add("JZ R0 -2;");
    list.add("RET R0;");
    m.execute(list);
   }
   catch(InvalidInstructionException | NoReturnValueException e){
      fail("Should not have thrown any exception");
   }
}
//JMP pc+val = progLength;(On point)
@Test public void JZ_Value_Boundary7()
{ 
      //execute code that you expect not to throw Exceptions.
    List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("MOV R0 1;");
    list.add("MOV R1 2;");
    list.add("MOV R2 1;");
    list.add("JZ R0 2;");
    list.add("ADD R1 R1 R2;");
    list.add("RET R1;");
    assertEquals(3, m.execute(list));

}
@Test public void JZ_Value_Boundary8()
{ 
      //execute code that you expect not to throw Exceptions.
    List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("MOV R0 0;");
    list.add("MOV R1 2;");
    list.add("MOV R2 1;");
    list.add("JZ R0 2;");
    list.add("ADD R1 R1 R2;");
    list.add("RET R1;");
    assertEquals(2, m.execute(list));

}
//JMP pc+val = progLength+1;(Off point)
@Test public void JZ_Value_Boundary9()
{ 
      //execute code that you expect not to throw Exceptions.
    List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("MOV R0 0;");
    list.add("MOV R1 2;");
    list.add("MOV R2 1;");
    list.add("JZ R0 3;");
    list.add("ADD R1 R1 R2;");
    list.add("RET R1;");
    thrown.expect(NoReturnValueException.class);
    m.execute(list);
}
@Test public void JZ_Value_Boundary10()
{ 
      //execute code that you expect not to throw Exceptions.
    List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("MOV R0 1;");
    list.add("MOV R1 2;");
    list.add("MOV R2 1;");
    list.add("JZ R0 3;");
    list.add("ADD R1 R1 R2;");
    list.add("RET R1;");
    assertEquals(3, m.execute(list));
}
  @Test(timeout = 3000) public void JZ_R0_0() 
  {
    List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("MOV R0 1;");
    list.add("MOV R1 2;");
    list.add("MOV R2 1;");
    list.add("JZ R0 0;");
    list.add("ADD R1 R1 R2;");
    list.add("RET R1;");
    m.execute(list);
  }
//@Test(timeout = 3000) public void JZ_R0_0_2() 
//  {
//    List<String> list = new ArrayList<>();
//    Machine m =new Machine();
//    list.add("MOV R0 0;");
//    list.add("MOV R1 2;");
//    list.add("MOV R2 1;");
//    list.add("JZ R0 0;");
//    list.add("ADD R1 R1 R2;");
//    list.add("RET R1;");
//    m.execute(list);
//  }
@Test public void JZ_X()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("JZ R0;");
    thrown.expect(InvalidInstructionException.class);
    m.execute(list);
}
}
