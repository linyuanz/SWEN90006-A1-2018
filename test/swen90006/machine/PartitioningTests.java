package swen90006.machine;

import java.util.List;
import java.util.ArrayList;
import org.junit.*;
import static org.junit.Assert.*;
import org.junit.Rule;
import org.junit.rules.ExpectedException;

public class PartitioningTests
{
  //Any method annotated with "@Before" will be executed before each test,
  //allowing the tester to set up some shared resources.
  @Before public void setUp()
  {
  }

  //Any method annotated with "@After" will be executed after each test,
  //allowing the tester to release any shared resources used in the setup.
  @After public void tearDown()
  {
  }

  //Any method annotation with "@Test" is executed as a test.
@Rule
public ExpectedException thrown= ExpectedException.none();

//ADD R10 R-1 R10;(Off point)
@Test public void Add_Regsiter_1()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("add R10 R-5 R10;");
    list.add("ret R10;");
    thrown.expect(InvalidInstructionException.class);
    m.execute(list);
}
//ADD R10 R32 R10;(Off point)
@Test public void Add_Regsiter_2()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("add R10 r40 R10;");
    list.add("ret R10;");
    thrown.expect(InvalidInstructionException.class);
    m.execute(list);
}
//ADD R10 R10 R20;(On point)
@Test
public void Add_Regsiter_3(){
   try{
      //execute code that you expect not to throw Exceptions.
      List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("add R10 R20 R10;");
    list.add("ret R10;");
    m.execute(list);
   }
   catch(InvalidInstructionException | NoReturnValueException e){
      fail("Should not have thrown any exception");
   }
}
@Test public void ADD_Value_1()
{ 
      //execute code that you expect not to throw Exceptions.
    List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("MOV R10 1;");
    list.add("MOV R1 2;");
    list.add("MOV R2 1;");
    list.add("ADD R1 R1 R2;");
    list.add("RET R1;");
    assertEquals(3, m.execute(list));
}
//MOV R-1  200;(Off point)
@Test public void ADD_X()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("ADD R0;");
    thrown.expect(InvalidInstructionException.class);
    m.execute(list);
}
//SUB R10 R32 R10;(Off point)
@Test public void Sub_Regsiter_1()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("sub R10 r40 R10;");
    list.add("ret R10;");
    thrown.expect(InvalidInstructionException.class);
    m.execute(list);
}
//SUB R10 R-1 R10;(Off point)
@Test public void Sub_Regsiter_2()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("Sub R10 r-5 R10;");
    list.add("ret R10;");
    thrown.expect(InvalidInstructionException.class);
    m.execute(list);
}
//SUB R10 R10 R20;(On point)
@Test public void Sub_Regsiter_3(){
   try{
      //execute code that you expect not to throw Exceptions.
    List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("sub R10 R20 R10;");
    list.add("ret R10;");
    m.execute(list);
   }
   catch(InvalidInstructionException | NoReturnValueException e){
      fail("Should not have thrown any exception");
   }
}
@Test public void Sub_Value_1()
{ 
      //execute code that you expect not to throw Exceptions.
    List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("MOV R10 1;");
    list.add("MOV R1 2;");
    list.add("MOV R2 1;");
    list.add("SUB R1 R1 R2;");
    list.add("RET R1;");
    assertEquals(1, m.execute(list));
}
@Test public void SUB_X()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("SUB R0;");
    thrown.expect(InvalidInstructionException.class);
    m.execute(list);
}
//Mul R10 R32 R10;(Off point)
@Test public void Mul_Regsiter_1()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("Mul R10 r40 R10;");
    list.add("ret R10;");
    thrown.expect(InvalidInstructionException.class);
    m.execute(list);
}
//mUL R10 R-1 R10;(Off point)
@Test public void Mul_Regsiter_2()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("Mul R10 r-5 R10;");
    list.add("ret R10;");
    thrown.expect(InvalidInstructionException.class);
    m.execute(list);
}
//SUB R10 R10 R20;(On point)
@Test public void Mul_Regsiter_3(){
   try{
      //execute code that you expect not to throw Exceptions.
    List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("Mul R10 R20 R10;");
    list.add("ret R10;");
    m.execute(list);
   }
   catch(InvalidInstructionException | NoReturnValueException e){
      fail("Should not have thrown any exception");
   }
}
@Test public void Mul_Value_1()
{ 
      //execute code that you expect not to throw Exceptions.
    List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("MOV R10 1;");
    list.add("MOV R1 2;");
    list.add("MOV R2 1;");
    list.add("Mul R1 R1 R2;");
    list.add("RET R1;");
    assertEquals(2, m.execute(list));
}
@Test public void MUL_X()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("MUL R0;");
    thrown.expect(InvalidInstructionException.class);
    m.execute(list);
}
//Div R10 R32 R10;(Off point)
@Test public void Div_Regsiter_1()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("Div R10 r40 R10;");
    list.add("ret R10;");
    thrown.expect(InvalidInstructionException.class);
    m.execute(list);
}
//Div R10 R-1 R10;(Off point)
@Test public void Div_Regsiter_2()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("Div R10 r-5 R10;");
    list.add("ret R10;");
    thrown.expect(InvalidInstructionException.class);
    m.execute(list);
}
//DIV R10 R10 R20;(On point)
@Test public void Div_Regsiter_3(){
   try{
      //execute code that you expect not to throw Exceptions.
    List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("mov r10 2");
    list.add("div R10 R20 R10;");
    list.add("ret R10;");
    m.execute(list);
   }
   catch(InvalidInstructionException | NoReturnValueException e){
      fail("Should not have thrown any exception");
   }
}
@Test public void Div_Value_1()
{ 
      //execute code that you expect not to throw Exceptions.
    List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("MOV R10 1;");
    list.add("MOV R1 4;");
    list.add("MOV R2 2;");
    list.add("DIV R1 R1 R2;");
    list.add("RET R1;");
    assertEquals(2, m.execute(list));
}
@Test public void Div_Value_2()
{ 
      //execute code that you expect not to throw Exceptions.
    List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("MOV R10 0;");
    list.add("MOV R1 4;");
    list.add("MOV R2 2;");
    list.add("DIV R1 R1 R10;");
    list.add("RET R1;");
    m.execute(list);
}
@Test public void DIV_X()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("DIV R0 R1;");
    thrown.expect(InvalidInstructionException.class);
    m.execute(list);
}
//RET R-1;(Off point)
@Test public void Ret_Regsiter_1()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("ret r-5;");
    thrown.expect(InvalidInstructionException.class);
    m.execute(list);
}
//RET R32;(Off point)
@Test public void Ret_Regsiter_2()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("ret r40;");
    thrown.expect(InvalidInstructionException.class);
    m.execute(list);
}
//RET R10;(On point)
@Test public void Ret_Regsiter_3(){
   try{
      //execute code that you expect not to throw Exceptions.
      List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("ret R10;");
    m.execute(list);
   }
   catch(InvalidInstructionException | NoReturnValueException e){
      fail("Should not have thrown any exception");
   }
}
//RET R20;(On point)
//@Test public void Ret_Regsiter_4(){
//   try{
//      //execute code that you expect not to throw Exceptions.
//      List<String> list = new ArrayList<>();
//    Machine m =new Machine();
//    list.add("ret R20 R31;");
//    thrown.expect(InvalidInstructionException.class);
//    m.execute(list);
//   }
//   catch(NoReturnValueException e){
//      fail("Should not have thrown any exception");
//   }
//}
//MOV R-1  200;(Off point)
@Test public void Mov_Regsiter_1()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("MOV R-1 200;");
    thrown.expect(InvalidInstructionException.class);
    m.execute(list);
}
//MOV R-1  200;(Off point)
@Test public void Mov_Regsiter_X()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("MOV R0;");
    thrown.expect(InvalidInstructionException.class);
    m.execute(list);
}
//MOV R10 200;(On point)
@Test public void Mov_Regsiter_2()
{ 
    try{
      //execute code that you expect not to throw Exceptions.
      List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("MOV R10 200;");
    list.add("Ret R10;");
    m.execute(list);
   }
   catch(InvalidInstructionException | NoReturnValueException e){
      fail("Should not have thrown any exception");
   }
}
//MOV R20 200;(On point)
@Test public void Mov_Regsiter_3()
{ 
    try{
      //execute code that you expect not to throw Exceptions.
      List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("MOV R20 200;");
    list.add("Ret R10;");
    m.execute(list);
   }
   catch(InvalidInstructionException | NoReturnValueException e){
      fail("Should not have thrown any exception");
   }
}
//MOV R32  200;(Off point)
@Test public void Mov_Regsiter_4()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("MOV R32 200;");
    thrown.expect(InvalidInstructionException.class);
    m.execute(list);
}
//MOV R10 65536;(Off point)
@Test public void Mov_Value_1()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("MOV R10 65536;");
    thrown.expect(InvalidInstructionException.class);
    m.execute(list);
}
//MOV R10  -65536;(Off point)
@Test public void Mov_Value_2()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("MOV R10  -65536;");
    thrown.expect(InvalidInstructionException.class);
    m.execute(list);
}
//MOV R10  -200;(On point)
@Test public void Mov_Value_3()
{ 
    try{
      //execute code that you expect not to throw Exceptions.
      List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("MOV R10  -200;");
    list.add("Ret R10;");
    m.execute(list);
   }
   catch(InvalidInstructionException | NoReturnValueException e){
      fail("Should not have thrown any exception");
   }
}
//LDR R1 R-1  200;(Off point)
@Test public void LDR_Regsiter_1()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("LDR R1 R-1  200;");
    thrown.expect(InvalidInstructionException.class);
    m.execute(list);
}
//LDR R1 R10 200;(On point)
@Test public void LDR_Regsiter_2()
{ 
    try{
      //execute code that you expect not to throw Exceptions.
      List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("LDR R1 R10 200;");
    list.add("Ret R1;");
    m.execute(list);
   }
   catch(InvalidInstructionException | NoReturnValueException e){
      fail("Should not have thrown any exception");
   }
}
//LDR R1 R20  200;(On point)
@Test public void LDR_Regsiter_3()
{ 
    try{
      //execute code that you expect not to throw Exceptions.
      List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("LDR R1 R20  200;");
    list.add("Ret R1;");
    m.execute(list);
   }
   catch(InvalidInstructionException | NoReturnValueException e){
      fail("Should not have thrown any exception");
   }
}
//LDR R1 R32  200;(Off point)
@Test public void LDR_Regsiter_4()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("LDR R1 R32  200;");
    thrown.expect(InvalidInstructionException.class);
    m.execute(list);
}
//LDR R1 R10  -65536;(Off point)
@Test public void LDR_Value_1()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("R1 R10  -65536;");
    thrown.expect(InvalidInstructionException.class);
    m.execute(list);
}
//LDR R1 R10  -200;(On point)
@Test public void LDR_Value_2()
{ 
    try{
      //execute code that you expect not to throw Exceptions.
      List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("MOV R10  -200;");
    list.add("Ret R10;");
    m.execute(list);
   }
   catch(InvalidInstructionException | NoReturnValueException e){
      fail("Should not have thrown any exception");
   }
}
//LDR R1 R10  65536;(On point)
@Test public void LDR_Value_3()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("R1 R10  65536;");
    list.add("Ret R1;");
    thrown.expect(InvalidInstructionException.class);
    m.execute(list);
}
//b+val<0
@Test public void LDR_Value_4()
{ 
    try{
      //execute code that you expect not to throw Exceptions.
      List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("MOV R20 1;");
    list.add("LDR R10 R20 -2;");
    list.add("RET R10;");
    m.execute(list);
   }
   catch(InvalidInstructionException | NoReturnValueException e){
      fail("Should not have thrown any exception");
   }
}
//b+val=0
@Test public void LDR_Value_5()
{ 
    try{
      //execute code that you expect not to throw Exceptions.
      List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("MOV R20 1;");
    list.add("LDR R10 R20 -1;");
    list.add("RET R10;");
    m.execute(list);
   }
   catch(InvalidInstructionException | NoReturnValueException e){
      fail("Should not have thrown any exception");
   }
}
// b+val=200
@Test public void LDR_Value_6()
{ 
    try{
      //execute code that you expect not to throw Exceptions.
      List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("MOV R20 0;");
    list.add("LDR R10 R20 200;");
    list.add("RET R10;");
    m.execute(list);
   }
   catch(InvalidInstructionException | NoReturnValueException e){
      fail("Should not have thrown any exception");
   }
}
// b+val=65536
@Test public void LDR_Value_7()
{ 
    try{
      //execute code that you expect not to throw Exceptions.
      List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("MOV R20 1;");
    list.add("LDR R10 R20 200;");
    list.add("RET R10;");
    m.execute(list);
   }
   catch(InvalidInstructionException | NoReturnValueException e){
      fail("Should not have thrown any exception");
   }
}
@Test public void LDR_X()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("LDR R0 R1;");
    thrown.expect(InvalidInstructionException.class);
    m.execute(list);
}
//JMP -65536;(Off point)
@Test public void Jmp_Value_1()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("JMP -65536;");
    thrown.expect(InvalidInstructionException.class);
    m.execute(list);
}
//JMP -200;(On point)
@Test public void JMP_Value_2()
{  List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("JMP -200;");
    thrown.expect(NoReturnValueException.class);
    m.execute(list);
}
//JMP 200(On point)
@Test public void JMP_Value_3()
{  List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("JMP 200;");
    thrown.expect(NoReturnValueException.class);
    m.execute(list);
}
//JMP 65536;(Off point)
@Test public void Jmp_Value_4()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("JMP 65536;");
    thrown.expect(InvalidInstructionException.class);
    m.execute(list);
}
//JMP pc+val = -1;(Off point)
@Test public void JMP_Value_5()
{  List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("MOV R10 1;");
    list.add("JMP -2;");
    list.add("RET R10;");
    thrown.expect(NoReturnValueException.class);
    m.execute(list);
}
//JMP pc+val = progLength;(On point)
@Test public void JMP_Value_6()
{ 
    try{
      //execute code that you expect not to throw Exceptions.
      List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("MOV R10 1;");
    list.add("JMP 1;");
    list.add("RET R10;");
    m.execute(list);
   }
   catch(InvalidInstructionException | NoReturnValueException e){
      fail("Should not have thrown any exception");
   }
}
//JMP pc+val = progLength+1;(Off point)
@Test public void JMP_Value_7()
{  List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("MOV R10 1;");
    list.add("JMP 2;");
    list.add("RET R10;");
    thrown.expect(NoReturnValueException.class);
    m.execute(list);
}
@Test public void JMP_X()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("JMP R0;");
    thrown.expect(InvalidInstructionException.class);
    m.execute(list);
}
//JZ R-1 200;(Off point)
@Test public void JZ_Regsiter_1()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("JZ R-1 200;");
    thrown.expect(InvalidInstructionException.class);
    m.execute(list);
}
//JZ R10 200;(On point)
@Test public void JZ_Regsiter_2()
{ 
    try{
      //execute code that you expect not to throw Exceptions.
      List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("mov R10 1;");
    list.add("JZ R10 200;");
    list.add("RET R10;");
    m.execute(list);
   }
   catch(InvalidInstructionException | NoReturnValueException e){
      fail("Should not have thrown any exception");
   }
}
@Test public void JZ_Regsiter_3()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("mov R10 0;");
    list.add("JZ R10 200;");
    list.add("RET R10;");
    thrown.expect(NoReturnValueException.class);
    m.execute(list);
}
//JZ R20 200;(On point)
@Test public void JZ_Regsiter_4()
{ 
    try{
      //execute code that you expect not to throw Exceptions.
      List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("mov R20 1;");
    list.add("JZ R20 200;");
    list.add("RET R20;");
    m.execute(list);
   }
   catch(InvalidInstructionException | NoReturnValueException e){
      fail("Should not have thrown any exception");
   }
}
@Test public void JZ_Regsiter_5()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("mov R20 0;");
    list.add("JZ R20 200;");
    list.add("RET R20;");
    thrown.expect(NoReturnValueException.class);
    m.execute(list);
}
//JZ R32 200;(Off point)
@Test public void JZ_Regsiter_6()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("JZ R32 200;");
    thrown.expect(InvalidInstructionException.class);
    m.execute(list);
}
//JZ R10 65536;(Off point)
@Test public void JZ_Value_1()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("MOV R10 0;");
    list.add("JZ R10 65536;");
    list.add("RET R20;");
    thrown.expect(InvalidInstructionException.class);
    m.execute(list);
}
//JZ R10 -200
@Test public void JZ_Value_2()
{ 
    try{
      //execute code that you expect not to throw Exceptions.
      List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("mov R20 1;");
    list.add("JZ R20 -200;");
    list.add("RET R20;");
    m.execute(list);
   }
   catch(InvalidInstructionException | NoReturnValueException e){
      fail("Should not have thrown any exception");
   }
}
@Test public void JZ_Value_3()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("mov R20 0;");
    list.add("JZ R20 -200;");
    list.add("RET R20;");
    thrown.expect(NoReturnValueException.class);
    m.execute(list);
}
//JZ R10 -65536;(Off point)
@Test public void JZ_Value_4()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("MOV R10 0;");
    list.add("JZ R10 -65536;");
    list.add("RET R20;");
    thrown.expect(InvalidInstructionException.class);
    m.execute(list);
}
//JZ R10 pc+val = -1;(Off point)
@Test public void JZ_Value_5()
{  List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("MOV R10 0;");
    list.add("JZ R10 -2;");
    list.add("RET R10;");
    thrown.expect(NoReturnValueException.class);
    m.execute(list);
}
//JZ R10 pc+val = -1;(Off point)
@Test public void JZ_Value_6()
{ 
    try{
      //execute code that you expect not to throw Exceptions.
      List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("MOV R10 1;");
    list.add("JZ R10 -2;");
    list.add("RET R10;");
    m.execute(list);
   }
   catch(InvalidInstructionException | NoReturnValueException e){
      fail("Should not have thrown any exception");
   }
}
//JMP pc+val = progLength;(On point)
@Test public void JZ_Value_7()
{ 
      //execute code that you expect not to throw Exceptions.
    List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("MOV R10 1;");
    list.add("MOV R1 2;");
    list.add("MOV R2 1;");
    list.add("JZ R10 2;");
    list.add("ADD R1 R1 R2;");
    list.add("RET R1;");
    assertEquals(3, m.execute(list));

}
@Test public void JZ_Value_8()
{ 
      //execute code that you expect not to throw Exceptions.
    List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("MOV R10 0;");
    list.add("MOV R1 2;");
    list.add("MOV R2 1;");
    list.add("JZ R10 2;");
    list.add("ADD R1 R1 R2;");
    list.add("RET R1;");
    assertEquals(2, m.execute(list));

}
//JMP pc+val = progLength+1;(Off point)
@Test public void JZ_Value_9()
{ 
      //execute code that you expect not to throw Exceptions.
    List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("MOV R10 0;");
    list.add("MOV R1 2;");
    list.add("MOV R2 1;");
    list.add("JZ R10 3;");
    list.add("ADD R1 R1 R2;");
    list.add("RET R1;");
    thrown.expect(NoReturnValueException.class);
    m.execute(list);
}
@Test public void JZ_Value_10()
{ 
      //execute code that you expect not to throw Exceptions.
    List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("MOV R10 1;");
    list.add("MOV R1 2;");
    list.add("MOV R2 1;");
    list.add("JZ R10 3;");
    list.add("ADD R1 R1 R2;");
    list.add("RET R1;");
    assertEquals(3, m.execute(list));
}
@Test public void JZ_X()
{ List<String> list = new ArrayList<>();
    Machine m =new Machine();
    list.add("JZ R0;");
    thrown.expect(InvalidInstructionException.class);
    m.execute(list);
}
}
